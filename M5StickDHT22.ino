/*  
 *   "ESP32 Pico Kit" board should be selected in Tools->Board
 */
#include <M5StickC.h>
#include <SimpleDHT.h>


SimpleDHT22 dht;

void setup(void) {
  M5.begin();
  M5.Lcd.setRotation(3);
}

void loop() {
  float temp, humid;
  int status = dht.read2(26, &temp, &humid, NULL);

  M5.Lcd.fillScreen(TFT_BLACK);
  M5.Lcd.drawRoundRect(4, 2, 154, 76, 6, TFT_WHITE); // adjusted for miss-aligned display
  M5.Lcd.setTextFont(4);
  M5.Lcd.setTextColor(TFT_WHITE,TFT_BLACK);  
  M5.Lcd.setTextSize(1);
  
  if (status == SimpleDHTErrSuccess) { // just frame shows if there is an issue with sensor
    M5.Lcd.setCursor(23, 14);
    M5.Lcd.setTextColor(TFT_RED, TFT_BLACK);  
    M5.Lcd.print("T "); M5.Lcd.print(temp); M5.Lcd.print("  C");
    // two lines below draw degree sign
    M5.Lcd.drawCircle(110, 14+6, 4, TFT_RED);
    M5.Lcd.drawCircle(110, 14+6, 3, TFT_RED);
    
    M5.Lcd.setTextColor(TFT_BLUE, TFT_BLACK);  
    M5.Lcd.setCursor(22, 44);
    M5.Lcd.print("H "); M5.Lcd.print(humid); M5.Lcd.print(" %");   
  }
  delay(500);
}
